const rewireEslint = require('react-app-rewire-eslint');
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = function override(config, env) {
    config = rewireEslint(config, env);
      if (env === 'development') {
        config.plugins.push(
          new StylelintPlugin({
            // options here
          })
        )
      }
    return config;
}
