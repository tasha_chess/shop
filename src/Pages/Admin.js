import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Button } from 'antd';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { loggedApp } from '../reducers/action';

const propTypes = {
  users: PropTypes.object,
  changeLog: PropTypes.func,
};

const Admin = (props) => {
  const {
    users,
    changeLog,
  } = props;

  console.log(users);

  return (
    <Row className="wrapper">
      <Col span={24}>
        { (users.autorization.isAuthorized) ? (
          <div>
           Welcome
            <Button
              onClick={() => {
                changeLog();
              }}
            >
             Log out
            </Button>
          </div>
        ) : (
          <div>
            You are not authorized
            <Link href="/login/" to="/login">Log In</Link>
          </div>
        )}

      </Col>
    </Row>
  );
};

Admin.propTypes = propTypes;

const mapStateToProps = state => ({
  users: state.shop.users,
});

const mapDispatchToProps = dispatch => ({
  changeLog: bindActionCreators(loggedApp, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
