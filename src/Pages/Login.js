import React from 'react';
import { PropTypes } from 'prop-types';
import { Layout, Row, Col, Card } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import LoginForm from '../Components/LoginForm';

const {
  Content,
} = Layout;

const propTypes = {
  users: PropTypes.object,
};

const Login = (props) => {
  const {
    users,
  } = props;

  if (users.autorization.isAuthorized === true) {
    return (
      <Content>
        <Row style={{ padding: '16px' }}>
          <Col span={24}>
            <Card
              title={<span>Welcome</span>}
            >
              SOMETHING
            </Card>
          </Col>
        </Row>
        <Row style={{ padding: '16px' }}>
          <Col>
            <Link to="registration" href="/registration">Create new account</Link>
          </Col>
        </Row>
      </Content>
    );
  }

  return (
    <Content>
      <Row style={{ padding: '16px' }}>
        <Col span={24}>
          <Card
            title={<span>Welcome</span>}
          >
            <LoginForm />
          </Card>
        </Col>
      </Row>
    </Content>
  );
};

Login.propTypes = propTypes;

const mapStateToProps = state => (
  {
    users: state.shop.users,
  }
);

// const mapDispatchToProps = dispatch => (
//   {
//
//   }
// );

export default connect(mapStateToProps)(Login);
