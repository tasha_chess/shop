import React from 'react';
import { Row, Col, Table, Button, Divider, Icon } from 'antd';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { deleteAll, deleteFromCart, decrementItems, incrementItems } from '../reducers/action';

const propTypes = {
  cart: PropTypes.object,
  products: PropTypes.object,
  removeProducts: PropTypes.func,
  removeProduct: PropTypes.func,
  decrementProduct: PropTypes.func,
  incrementProduct: PropTypes.func,
};

const Cart = (props) => {
  const {
    cart,
    products,
    removeProducts,
    removeProduct,
    decrementProduct,
    incrementProduct,
  } = props;

  console.log(props);

  if (cart.isEmpty) {
    return (
      <Row className="wrapper cart">
        <Col span={24}>
          <span>Cart is empty</span>
        </Col>
      </Row>
    );
  }

  const actionRender = id => (
    <span>
      <Button onClick={() => {
        incrementProduct(id);
      }}
      >
        <Icon type="plus" />
      </Button>
      <Divider type="vertical" />
      <Button onClick={() => {
        decrementProduct(id);
      }}
      >
        <Icon type="minus" />
      </Button>
      <Divider type="vertical" />
      <Button onClick={() => {
        removeProduct(id);
      }}
      >
        Delete
      </Button>
    </span>
  );
  const renderCarColumn = car => (
    <div>
      <img src={car.img} alt={car.model} className="cart__table-image" />
      <span>{car.model}</span>
    </div>
  );

  const columns = [{
    title: 'Car',
    dataIndex: 'car',
    render: renderCarColumn,
  }, {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
  }, {
    title: 'Count',
    dataIndex: 'count',
    key: 'count',
  }, {
    title: 'Actions',
    dataIndex: 'actions',
    render: actionRender,
  }];

  const ids = Object.keys(cart.products);

  const data = ids.map((id) => {
    const item = products[id];

    return {
      key: id,
      car: {
        model: item.model,
        img: item.img,
      },
      price: item.price,
      actions: id,
      count: cart.products[id].count,
    };
  });

  return (
    <Row className="cart wrapper">
      <Col span={24}>
        <Table
          className="cart__table"
          dataSource={data}
          columns={columns}
          pagination={{ position: 'none' }}
          bordered
          footer={() => (
            <div>
              <div>
                <Button
                  type="primary"
                  onClick={() => {
                    removeProducts();
                  }}
                >
                  Remove all
                </Button>
              </div>
              <div>
                Subtotal: {cart.subtotal}
              </div>
            </div>
          )
          }
        />

      </Col>
    </Row>
  );
};

Cart.propTypes = propTypes;

const mapStateToProps = state => (
  {
    cart: state.shop.cart,
    products: state.shop.products,
  }
);

const mapDispatchToProps = dispatch => (
  {
    removeProducts: bindActionCreators(deleteAll, dispatch),
    removeProduct: bindActionCreators(deleteFromCart, dispatch),
    incrementProduct: bindActionCreators(incrementItems, dispatch),
    decrementProduct: bindActionCreators(decrementItems, dispatch),
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
