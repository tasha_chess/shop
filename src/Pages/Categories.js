import React from 'react';
import { Row, Col, Card } from 'antd';
import { Link } from 'react-router-dom';
import data from '../data.json';

export const Categories = () => {
  const ids = data.category.categoryIds;
  const categories = data.category;

  const cats = ids.map((id) => {
    const item = categories[id];

    return (
      <Col lg={6} sm={12} md={8} xs={24} key={id}>
        <Card
          hoverable
          cover={<img src={item.img} alt={item.mark} />}
          title={<Link to={item.link} href={item.link}>{item.mark}</Link>}
        >
        </Card>
      </Col>
    );
  });

  return (
    <Row className="categories">
      {cats}
    </Row>
  );
};
