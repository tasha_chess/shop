import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getDataAction } from '../reducers/action';
import data from '../data.json';

class Ex extends React.Component {
  constructor() {
    super();
    this.state = {
      items: [],
      isLoading: true,
      hasErrored: false,
    };
  }

  delay(interval) {
    this.prom =  new Promise(function(resolve) {
      setTimeout(resolve, interval);
    });

    return this.prom;
  }


  render() {
    fetch('http://5826ed963900d612000138bd.mockapi.io/items')
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }

        this.setState({ isLoading: false });

        return response;
      })
      .then((response) => response.json())
      .then((items) => this.setState({ items }))
      .catch(() => this.setState({ hasErrored: true }));

    return (
      <div>dgsd</div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.shop.users,
});

const mapDispatchToProps = dispatch => ({
  getData: bindActionCreators(getDataAction, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Ex);
