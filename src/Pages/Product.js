import React from 'react';
import { Card, Col, Row, Button } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { addToCart, addCat } from '../reducers/action';
import { PageNotFound } from '../Components/PageNotFound';

const propTypes = {
  products: PropTypes.object,
  match: PropTypes.object,
  cart: PropTypes.object,
  addToCartBtn: PropTypes.func,
  addCatBtn: PropTypes.func,
};

const Product = (props) => {
  console.log(props);
  const {
    products,
    match,
    cart,
    addToCartBtn,
    addCatBtn,
  } = props;
  const mark = match.params.markName;
  const productId = products.productsIds[mark];
  const product = products[productId];

  console.log({
    ...props,
    cart: { c: 'vc' },
  }, cart, 'cart');
  if (productId === undefined) {
    return (
      <PageNotFound />
    );
  }

  return (
    <Row>
      <Col span={20}>
        <Card hoverable className="product">
          <div className="image">
            <img src={product.img} alt={product.model} />
          </div>
          <div className="description">
            <p>{product.decription}</p>
            <p>{`Generation ${product.generation}, Hp: ${product.hp}`}</p>
            <p>{`Price: ${product.price} C.U.`}</p>
            <Button
              onClick={() => {
                addToCartBtn(product.id);
              }}
              type="primary"
            >
              Add to cart
            </Button>
          </div>
        </Card>
        <p>
          <Button
            type="primary"
            onClick={
              () => {
                addCatBtn({
                  type: 'ADD_CAT',
                  mark: 'mersedes-benz',
                  link: '/catalog/mersedes/',
                });
              }
            }
          >
            add category
          </Button>
        </p>
      </Col>
    </Row>
  );
};

Product.propTypes = propTypes;

const mapStateToProps = state => ({
  products: state.shop.products,
  cart: state.shop.cart,
});

const mapDispatchToProps = dispatch => ({
  addToCartBtn: bindActionCreators(addToCart, dispatch),
  addCatBtn: bindActionCreators(addCat, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
