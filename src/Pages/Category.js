import React from 'react';
import { PropTypes } from 'prop-types';
import { Row, Col, Card, Button } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { PageNotFound } from '../Components/PageNotFound';
import { addToCart } from '../reducers/action';

const propTypes = {
  cats: PropTypes.object,
  match: PropTypes.object,
  addToCartBtn: PropTypes.func,
};

const Category = (props) => {
  const {
    match,
    cats,
    addToCartBtn,
  } = props;
  const model = match.params.catalogName;
  let ids = 0;

  try {
    ids = cats.category[model].products;
  } catch (e) {
    console.log(`${e.name}: ${e.message}`);
  }

  if (!ids) {
    return (
      <PageNotFound />
    );
  }

  const cars = ids.map((id) => {
    const item = cats.products[id];

    return (
      <Col lg={6} xs={24} sm={12} md={8} key={item.id}>
        <Card
          hoverable
          cover={<img src={item.img} alt="accord" />}
        >
          <Link to={`/catalog/${model}/${item.alias}`} href={`/catalog/${model}/${item.alias}`}>{item.model}</Link>
          <p>{`Generation ${item.generation}, Hp: ${item.hp}`}</p>
          <p>{(item.price) ? `Price: ${item.price} C.U.` : 'Specify a price'}</p>
          <Button
            type="primary"
            onClick={() => {
              addToCartBtn(item.id);
            }}
          >
            Add to cart
          </Button>
        </Card>
      </Col>
    );
  });

  return (
    <div>
      <Row className="category">
        {cars}
      </Row>
    </div>
  );
};

Category.propTypes = propTypes;

const mapStateToProps = state => ({
  cats: state.shop,
});

const mapDispatchToProps = dispatch => ({
  addToCartBtn: bindActionCreators(addToCart, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Category);
