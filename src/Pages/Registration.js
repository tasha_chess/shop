import React from 'react';
import { Form, Input, Row, Col, Layout, Card, Button } from 'antd';
// import {Tooltip, Icon, Cascader, Select, Checkbox, Button, AutoComplete}
//
// const {
//   FormItem,
// } = Form.Item;

const {
  Content,
} = Layout;

export const Registration = () => {
  return (
    <Content>
      <Row style={{ padding: '16px' }}>
        <Col span={24}>
          <Card style={{ width: '90%', margin: '0 auto', maxWidth: '900px' }}>
            <Form className="registration">
              <Input type="text" placeholder="name" />
              <Input type="text" placeholder="surname" />
              <Input type="text" placeholder="mail@mail.ru" />
              <Input type="text" placeholder="username" />
              <Input type="text" placeholder="phone number" />
              <Input type="text" placeholder="location" />
              <Button type="primary">Send</Button>
            </Form>
          </Card>
        </Col>
      </Row>
    </Content>
  );
};

