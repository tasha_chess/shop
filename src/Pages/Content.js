import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';
import { Categories } from './Categories';
import Category from './Category';
import Product from './Product';

const {
  Content,
} = Layout;

export const ContentComponent = () => {
  return (
    <Content>
      <Switch>
        <Route exact path="/catalog" component={Categories} />
        <Route exact path="/catalog/:catalogName" component={Category} />
        <Route path="/catalog/:catalogName/:markName" component={Product} />
      </Switch>
    </Content>
  );
};
