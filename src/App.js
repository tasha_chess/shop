import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
//import thunk from 'redux-thunk';
import { addCat } from './reducers/action';
import HeaderComponent from './Components/Header';
import { FooterComponent } from './Components/Footer';
import { Registration } from './Pages/Registration';
import Ex from './Pages/Ex';
import { Catalog } from './Components/Catalog';
import { Home } from './Pages/Home';
import Login from './Pages/Login';
import Admin from './Pages/Admin';
import Cart from './Pages/Cart';
import './App.css';

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <div>
          <HeaderComponent />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/admin" component={Admin} />
            <Route path="/catalog" component={Catalog} />
            <Route path="/login" component={Login} />
            <Route path="/registration" component={Registration} />
            <Route path="/order" component={Catalog} />
            <Route path="/cart" component={Cart} />
            <Route path="/admin/category" component={Catalog} />
            <Route path="/ex" component={Ex} />
          </Switch>
          <FooterComponent />
        </div>
      </BrowserRouter>
    </div>
  );
};

const mapStateToprops = (state) => {
  return {
    categories: state.categories,
  };
};

const mapDspatchToProps = dispatch => ({
  add: bindActionCreators(addCat, dispatch),
});

export default connect(mapStateToprops, mapDspatchToProps)(App);
