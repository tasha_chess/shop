export const initialState = {
  category: {
    honda: {
      mark: 'Honda',
      img: 'https://blogmedia.dealerfire.com/wp-content/uploads/sites/175/2015/01/honda-logo.png',
      link: '/catalog/honda',
      products: [
        '544437',
        '64634',
        '6565',
        '5454',
      ],
    },
    volvo: {
      mark: 'Volvo',
      img: 'http://www.carlogos.org/logo/Volvo-logo-2014-1920x1080.png',
      link: '/catalog/volvo',
      products: [
        '45534564',
        '5364',
      ],
    },
    subaru: {
      mark: 'Subaru',
      img: 'https://upload.wikimedia.org/wikipedia/ru/thumb/9/9f/Subaru_Logo.svg/1280px-Subaru_Logo.svg.png',
      link: '/catalog/subaru',
      products: [
        '45646',
        '65448',
        '5475646',
      ],
    },
    categoryIds: [
      'honda',
      'volvo',
      'subaru',
    ],
  },
  products: {
    544437: {
      id: '544437',
      alias: 'civic',
      model: 'Civic',
      generation: '5',
      hp: '125',
      decription: 'The fifth generation of the Honda Civic debuted in Japan on September 9, 1991. The new Civic was' +
      'larger than its predecessor, had a more aerodynamic body and the wheelbase was increased to 257 cm ' +
      '(101.3 inches) for the three-door hatchback and 262 cm (103.2 inches) for the four-door sedan. The ' +
      'wagon was also dropped for overseas markets, while the previous generation station wagon ("Shuttle")' +
      'continued in Japan and Europe',
      img: 'http://carakoom.com/data/blogs/689/11100/image/3.jpg',
      price: 10,
    },
    64634: {
      id: '64634',
      alias: 'accord',
      model: 'Accord',
      generation: '9',
      hp: '230',
      decription: 'The Honda Accord (Japanese: ホンダ・アコード Honda Akōdo) /əˈkɔːrd/ is a series of automobiles' +
      ' manufactured by Honda since 1976, best known for its four-door sedan variant, which has been one of the' +
      ' best-selling cars in the United States since 1989. The Accord nameplate has been applied to a variety ' +
      'of vehicles worldwide, including coupes, wagons, hatchbacks and a crossover.',
      img: 'https://fastmb.ru/uploads/posts/2017-11/1510869719_1.jpg',
    },
    6565: {
      id: '6565',
      alias: 'crv',
      model: 'CR-V',
      generation: '5',
      hp: '188',
      decription: 'The Honda CR-V is a compact crossover manufactured by Honda since 1995 and introduced in the ' +
      'North American market in 1997.[1][2] It uses the Honda Civic platform with an SUV body design. The CR-V ' +
      'is Honda\'s mid-range utility vehicle, slotting between the smaller HR-V and the larger Pilot. Honda ' +
      'states "CR-V” stands for "Comfortable Runabout Vehicle"[3][4][5] while the term "Compact Recreational ' +
      'Vehicle" is used in a British car review article that was republished by Honda',
      img: 'http://www.allcarz.ru/wp-content/uploads/2016/10/foto-crv-5_01.jpg',
      price: 14,
    },
    5454: {
      id: '5454',
      alias: 'integra',
      model: 'Integra',
      generation: '4',
      hp: '220',
      decription: 'The Honda Integra (sold in some markets as Acura Integra and Rover 416i) is an automobile produced' +
      ' by Japanese automobile manufacturer Honda from 1985 to 2006. It succeeded the Honda Quint as the slightly ' +
      'larger derivative of the Civic and each generation of the Integra was derived from the contemporary generation' +
      ' of the Civic. Being more luxurious and sports-oriented than the Quint, the Integra was one of the launch models' +
      ' for Acura in 1986,[1] along with the Legend. Throughout its life, the Integra was highly regarded ' +
      'for its handling and performance.',
      img: 'https://hips.hearstapps.com/roa.h-cdn.co/assets/15/44/1600x800/landscape-1445868943-roa110115dpt-bootcolin2.jpg?resize=768:*',
      price: 7,
    },
    45534564: {
      id: '45534564',
      alias: 'c30',
      model: 'C30',
      generation: '1',
      hp: '170',
      decription: 'The Volvo C30 is a three-door, front-engine, front-wheel-drive premium compact hatchback,' +
      ' manufactured and marketed by Volvo Cars for model years 2006-2013 in a single generation. Powered by' +
      ' inline-four and straight-five engines, the C30 is variant of the Volvo S40/V50/C70 range, sharing the' +
      ' same Ford C1/Volvo P1 platform. Volvo marketed the C30 as a premium hatchback / sports coupe.',
      img: 'https://a.d-cd.net/c32cf39s-960.jpg',
      price: 16,
    },
    5364: {
      id: '5364',
      alias: 's60',
      model: 'S60',
      generation: '2',
      hp: '215',
      decription: 'The Volvo S60 is a compact luxury sedan manufactured and marketed by Volvo since 2000 and is now' +
      'in its third generation.',
      img: 'https://a.d-cd.net/cd38a86s-960.jpg',
      price: 18,
    },
    45646: {
      id: '45646',
      alias: 'impreza_wrx',
      model: 'Impreza WRX',
      generation: '3',
      hp: '265',
      decription: 'The Subaru Impreza (スバル・インプレッサ) is a compact family car that has been manufactured since ' +
      '1992 by Subaru, introduced as a replacement for the Leone, with the predecessor\'s EA series engines replaced ' +
      'by the new EJ series.',
      img: 'https://a.d-cd.net/9e4c1f9s-1920.jpg',
      price: 20,
    },
    65448: {
      id: '65448',
      alias: 'forester',
      model: 'Forester',
      generation: '4',
      hp: '220',
      decription: 'The Subaru Forester is a station-wagon based compact crossover SUV manufactured since 1997 by ' +
      'Subaru. Available in Japan from 1997, the Forester shares its platform with the Impreza. It has been awarded ' +
      'Motor Trend\'s 2009 and 2014 SUV of the Year and The Car Connection\'s Best Car To Buy 2014.[2]',
      img: 'https://img.gazeta.ru/files3/275/8205275/upload-SF_008-pic905-895x505-66861.jpg',
      price: 19,
    },
    5475646: {
      id: '5475646',
      alias: 'legacy',
      model: 'Legacy',
      generation: '5',
      hp: '260',
      decription: 'The fifth-generation Subaru Legacy was originally unveiled as a concept car[1][2] at the 2009 ' +
      'North American International Auto Show in Detroit to commemorate the 20th anniversary of the model, and the' +
      'production version was introduced at the 2009 New York International Auto Show.[3] Production of the fifth' +
      'generation started on 29 May 2009.[4]',
      img: 'https://auto.ironhorse.ru/wp-content/uploads/2014/11/Legacy-5-old.jpg',
      price: 19,
    },
    productsIds: {
      civic: '544437',
      accord: '64634',
      crv: '6565',
      integra: '5454',
      c30: '45534564',
      s60: '5364',
      impreza_wrx: '45646',
      forester: '65448',
      legacy: '5475646',
    },
  },
  cart: {
    isEmpty: false,
    products: {
      6565: {
        count: 1,
      },
    },
    count: 1,
    subtotal: 14,
  },
  users: {
    tasha_chess: {
      password: '1234567',
      token: 'masklgmwadkglnakndgvbnd',
    },
    autorization: {
      isAuthorized: false,
    },
    userAction: {
      masklgmwadkglnakndgvbnd: {
        products: [],
        order: {},
      },
    },
  },
};
