export const addCat = obj => ({
  type: 'ADD_CATds',
  payload: obj,
});

export const addToCart = id => ({
  type: 'ADD_ITEM_TO_CART',
  payload: id,
});

export const deleteAll = () => ({
  type: 'DELETE_ALL_ITEMS',
});

export const loggedApp = () => ({
  type: 'CHANGE_LOG',
});

export const deleteFromCart = id => ({
  type: 'DELETE_ITEM',
  payload: id,
});

export const decrementItems = id => ({
  type: 'DECREMENT_ITEMS',
  payload: id,
});

export const incrementItems = id => ({
  type: 'INCREMENT_ITEMS',
  payload: id,
});

export const loginAction = obj => ({
  type: 'LOGIN',
  payload: obj,
});

export const getDataAction = obj => ({
  type: 'GET_DATA',
  payload: obj,
});

export const itemsHasErroed = bool => ({
  type: 'ITEMS_HAS_ERRORED',
  hasErrored: bool,
});

export const itemsIsLoading = bool => ({
  type: 'ITEMS_IS_LOADING',
  isLoading: bool,
});

export const itemsFetchDataSuccess = items => ({
  type: 'ITEMS_FETCH_DATA_SUCCESS',
  items,
});
