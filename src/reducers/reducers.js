import { combineReducers } from 'redux';
import { initialState } from './state';

const cartAction = (state, action) => {
  const newCart = Object.assign({}, state.cart);
  const products = Object.assign({}, state.products);
  const id = action.payload;
  const productPrice = products[id].price || 0;

  switch (action.type) {
    case 'ADD_ITEM_TO_CART': {
      newCart.count += 1;
      newCart.isEmpty = false;
      newCart.products[id] = newCart.products[id] || {};
      newCart.products[id].count = newCart.products[id].count || 0;
      newCart.products[id] = {
        count: newCart.products[id].count + 1,
      };
      newCart.subtotal += productPrice;

      return {
        ...state,
        cart: newCart,
      };
    }

    case 'INCREMENT_ITEMS': {
      newCart.products[id].count += 1;
      newCart.subtotal += productPrice;
      newCart.count += 1;

      return {
        ...state,
        cart: newCart,
      };
    }

    case 'DECREMENT_ITEMS': {
      if (newCart.products[id].count > 1) {
        newCart.products[id].count -= 1;
        newCart.subtotal -= productPrice;
        newCart.count -= 1;
      }

      return {
        ...state,
        cart: newCart,
      };
    }

    case 'DELETE_ITEM': {
      const prodCount = newCart.products[id].count;
      delete newCart.products[id];
      newCart.count -= prodCount;
      newCart.subtotal -= productPrice * prodCount;

      if (newCart.count <= 0) newCart.isEmpty = true;

      return {
        ...state,
        cart: newCart,
      };
    }

    default:
      return state;
  }
};

const loginAction = (state, action) => {
  const newUsers = Object.assign({}, state.users);

  switch (action.type) {
    case 'LOGIN': {
      const {
        name,
        pass,
      } = action.payload;

      if (newUsers[name]) {
        if (newUsers[name].password === pass) {
          newUsers.autorization.isAuthorized = true;
          newUsers.userAction = newUsers[name].token;
        }
      }

      return {
        ...state,
        users: newUsers,
      };
    }
    case 'CHANGE_LOG': {
      newUsers.autorization.isAuthorized = false;

      return {
        ...state,
        users: newUsers,
      };
    }
    default:
      return state;
  }
};

const shop = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ITEM_TO_CART':
      return cartAction(state, action);

    case 'DELETE_ITEM':
      return cartAction(state, action);

    case 'DECREMENT_ITEMS':
      return cartAction(state, action);

    case 'INCREMENT_ITEMS':
      return cartAction(state, action);

    case 'DELETE_ALL_ITEMS':
      return {
        ...state, cart: { ...state.cart, isEmpty: true, products: [] },
      };

    case 'LOGIN':
      return loginAction(state, action);

    case 'CHANGE_LOG':
      return loginAction(state, action);

    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  shop,
});
