import React from 'react';
import { Row, Col, Layout } from 'antd';

const { Footer } = Layout;

export const FooterComponent = () => {
  return (
    <Footer>
      <Row className="wrapper">
        <Col span={24}></Col>
      </Row>
    </Footer>
  );
};
