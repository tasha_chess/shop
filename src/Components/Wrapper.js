import React from 'react';
import { Switch, Route } from 'react-router-dom';
import '../App.css';
import { HeaderComponent } from './Header';
import { FooterComponent } from './Footer';
import { Home } from '../Pages/Home';
import { Admin } from '../Pages/Admin';
import { Catalog } from './Catalog';
import { Login } from '../Pages/Login';
import { Registration } from '../Pages/Registration';

export const Wrapper = () => {
  return (
    <div>
      <HeaderComponent />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/admin" component={Admin} />
        <Route path="/catalog" component={Catalog} />
        <Route path="/login" component={Login} />
        <Route path="/registration" component={Registration} />
        <Route path="/order" component={Catalog} />
        <Route path="/cart" component={Admin} />
        <Route path="/admin/category" component={Catalog} />
        <Route path="/admin/category/product" component={Catalog} />
      </Switch>
      <FooterComponent />
    </div>
  );
};

// export class Wrapper extends React.Component {
//   static propTypes = {
//     dispatch: React.PropTypes.func,
//   };
//
//   render() {
//     const {
//       dispatch,
//     } = this.props;
//
//     console.log(this.props);
//     console.log(dispatch);
//
//     return (
//       <div>
//         <HeaderComponent />
//         <Switch>
//           <Route exact path="/" component={Home} />
//           <Route path="/admin" component={Admin} />
//           <Route path="/catalog" component={Catalog} />
//           <Route path="/login" component={Login} />
//           <Route path="/registration" component={Registration} />
//           <Route path="/order" component={Catalog} />
//           <Route path="/cart" component={Admin} />
//           <Route path="/admin/category" component={Catalog} />
//           <Route path="/admin/category/product" component={Catalog} />
//         </Switch>
//       </div>
//     );
//   }
// }
