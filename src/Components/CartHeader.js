import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';

const propTypes = {
  cart: PropTypes.object,
};

const CartHeader = (props) => {
  const {
    cart,
  } = props;

  return (
    <div className="header__cart">
      <Icon type="shopping-cart" />
      {(cart.isEmpty) ? '0' : cart.count} product
      <Link to="/cart" href="/cart">Go to cart</Link>
    </div>
  );
};

CartHeader.propTypes = propTypes;

const mapStateToProps = state => (
  {
    cart: state.shop.cart,
  }
);

export default connect(mapStateToProps)(CartHeader);
