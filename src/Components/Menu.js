import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

export const MenuHeader = () => {
  return (
    <Menu
      theme="dark"
      mode="horizontal"
      defaultSelectedKeys={['2']}
      style={{ lineHeight: '64px' }}
    >
      <Menu.Item key="1"><Link to="/" href="/">Home</Link></Menu.Item>
      <Menu.Item key="2"><Link to="/catalog" href="/catalog"> Catalog </Link></Menu.Item>
      <Menu.Item key="3"><Link to="/about" href="/about"> About </Link></Menu.Item>
    </Menu>
  );
};

export default MenuHeader;
