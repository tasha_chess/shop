import React from 'react';
import { Icon, Input, Button } from 'antd';
import { PropTypes } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loginAction } from '../reducers/action';


const propTypes = {
  logIn: PropTypes.func,
};

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pass: '',
      name: '',
    };
  }

  componentDidMount() {
    console.log('fd');
  }

  render() {
    const {
      logIn,
    } = this.props;

    return (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Input
          style={{ width: '40%' }}
          type="text"
          prefix={<Icon type="user" placeholder="Username" />}
          onChange={event => (this.setState({
            name: event.target.value,
          }))}
        />
        <Input
          style={{ width: '40%' }}
          type="password"
          prefix={<Icon type="lock" placeholder="Password" />}
          onChange={event => (this.setState({
            pass: event.target.value,
          }))}
        />
        <Button
          onClick={() => (
            logIn({
              name: this.state.name,
              pass: this.state.pass,
            }))}
          style={{ width: '15%' }}
          type="primary"
          htmlType="submit"
        >
          Log in
        </Button>
      </div>
    );
  }
}

LoginForm.propTypes = propTypes;

const mapStateToProps = state => ({
  users: state.users,
});

const mapDispatchToProps = dispatch => ({
  logIn: bindActionCreators(loginAction, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
