import React from 'react';
import { Row, Col, Layout, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import MenuHeader from './Menu';
import CartHeader from './CartHeader';

const propTypes = {
  users: PropTypes.object,
};
const { Header } = Layout;

const HeaderComponent = (props) => {
  const {
    users,
  } = props;

  let auth = (
    <div>
      <Icon type="user" className="App-header__user-icon" />
      <Link to="/login" href="/login" >Login</Link>
    </div>
  );

  if (users.autorization.isAuthorized === true) {
    auth = (
      <div>
        <Icon type="smile" className="App-header__user-icon" style={{ color: 'yellow' }} />
        <Link to="/admin" href="/admin" >Admin</Link>
      </div>
    );
  }

  return (
    <Header className="App-header">
      <Row className="wrapper">
        <Col span={6}>
          {auth}
        </Col>
        <Col span={10}>
          <MenuHeader />
        </Col>
        <Col span={8}>
          <CartHeader />
        </Col>
      </Row>
    </Header>
  );
};

HeaderComponent.propTypes = propTypes;

const mapStateToProps = state => ({
  users: state.shop.users,
});

export default connect(mapStateToProps)(HeaderComponent);
