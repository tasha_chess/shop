import React from 'react';
import { Layout, Col, Row } from 'antd';
import { SiderComponent } from './Sider';
import { ContentComponent } from '../Pages/Content';

export const Catalog = () => {
  return (
    <div>
      <Layout className="catalog" style={{ width: '100%' }}>
        <Row gutter={16} className="wrapper">
          <Col lg={4} sm={24}>
            <SiderComponent />
          </Col>
          <Col lg={20} sm={24}>
            <ContentComponent />
          </Col>
        </Row>
      </Layout>
    </div>
  );
};
