import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';

const {
  Sider,
} = Layout;

export const SiderComponent = () => {
  return (
    <Sider
      style={{ width: 'auto' }}
      className="sider"
      width="100%"
      collapsedWidth="80"
    >
      <Menu
        theme="dark"
      >
        <Menu.Item key="020"><Link to="/catalog/honda" href="/catalog/honda">Honda</Link></Menu.Item>
        <Menu.Item key="021"><Link to="/catalog/volvo" href="/catalog/volvo">Volvo</Link></Menu.Item>
        <Menu.Item key="024"><Link to="/catalog/subaru" href="/catalog/subaru">Subaru</Link></Menu.Item>
      </Menu>
    </Sider>
  );
};
